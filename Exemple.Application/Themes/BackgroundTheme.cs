﻿using Kraken.Domain.Interfaces.Theme;

namespace Exemple.Application.Themes {

    public class BackgroundTheme: IBackgroundTheme {
        public string Primary => "white";
        public string Secondary => "gray";
        public string Dark => "darkgray";
    }
}