﻿using Kraken.Domain.Interfaces.Theme;

namespace Exemple.Application.Themes {

    public class TextTheme: ITextTheme {
        public string Primary => "black";
        public string Secondary => "white";
        public string Disabled => "darkgrey";
    }
}