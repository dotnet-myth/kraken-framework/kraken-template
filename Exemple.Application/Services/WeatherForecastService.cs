using Blazored.LocalStorage;
using Exemple.Core.Interfaces.Service;
using Exemple.Core.ViewModels.WeatherForecast;
using Kraken.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Exemple.Application.Services {

    public class WeatherForecastService: Service, IWeatherForecastService {

        private static readonly string[ ] Summaries = new[ ] {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherForecastService(
            HttpClient client,
            ILocalStorageService localStorageService )
            : base( client, localStorageService ) {
        }

        public Task<List<WeatherForecastViewModel>> GetForecastAsync( DateTime startDate ) {
            var rng = new Random( );
            return Task.FromResult( Enumerable.Range( 1, 5 ).Select( index => new WeatherForecastViewModel {
                Date = startDate.AddDays( index ),
                TemperatureC = rng.Next( -20, 55 ),
                Summary = Summaries[ rng.Next( Summaries.Length ) ]
            } ).ToList( ) );
        }
    }
}