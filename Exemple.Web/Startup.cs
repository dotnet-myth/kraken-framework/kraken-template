using Kraken.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Exemple.Web {

    public class Startup {
        public IConfiguration Configuration { get; }

        public Startup( IConfiguration configuration ) {
            Configuration = configuration;
        }

        public void ConfigureServices( IServiceCollection services ) {
            services.AddKraken( option => {
                option.HttpPort = 5000;
                option.HttpsPort = 5001;
                option.SpinnerType = BlazorPro.Spinkit.SpinnerType.Pulse;
            } );

            services.AddRazorPages( );
            services.AddServerSideBlazor( );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            if ( env.IsDevelopment( ) )
                app.UseDeveloperExceptionPage( );
            else
                app.UseExceptionHandler( "/Error" );
            app.UseHsts( );

            app.UseHttpsRedirection( );
            app.UseStaticFiles( );

            app.UseRouting( );

            app.UseKraken( );

            app.UseEndpoints( endpoints => {
                endpoints.MapBlazorHub( );
                endpoints.MapFallbackToPage( "/_Host" );
            } );
        }
    }
}