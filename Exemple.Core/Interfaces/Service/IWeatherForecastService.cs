﻿using Exemple.Core.ViewModels.WeatherForecast;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exemple.Core.Interfaces.Service {

    public interface IWeatherForecastService {

        Task<List<WeatherForecastViewModel>> GetForecastAsync( DateTime startDate );
    }
}